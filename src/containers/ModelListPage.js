import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Grid, Row} from 'react-bootstrap';
import Video from '../components/Video';
import Actor from '../components/Actor';
import SocialNetwork from '../components/SocialNetwork';
import {changeVideo} from '../actions/changeVideoAction';

export class ModelsListPage extends React.Component {

    render() {
        return (
            <div>
                <Grid>
                    <Row>
                        {this.props.data.videos.length > 0 &&
                        <Video
                            videos={this.props.data.videos}
                            currentVideo={this.props.currentVideo}
                            changeVideo={this.props.changeVideo}
                        />}
                        <Actor data={this.props.data.modelInfo} />
                    </Row>
                </Grid>
               <Grid>
                   <Row>
                       <SocialNetwork
                           data={this.props.data}
                           currentVideo={this.props.currentVideo}
                           changeVideo={this.props.changeVideo}
                       />
                   </Row>
               </Grid>
            </div>
        );
    }
}

ModelsListPage.propTypes = {
    data: PropTypes.object.isRequired,
    getVideo: PropTypes.func,
    currentVideo: PropTypes.object,
    changeVideo: PropTypes.func.isRequired
};

const mapStateToProps = ({modelListReducer}) => ({
    data: modelListReducer.data,
    currentVideo: modelListReducer.currentVideo
});

export default connect(
    mapStateToProps, {changeVideo}
)(ModelsListPage);