// Centralized propType definitions
import PropTypes from 'prop-types';

const { shape, number, string, object } = PropTypes;

export const showVideo = shape({
  newMpg: PropTypes.oneOf[number,string],
  tradeMpg: PropTypes.oneOf[number,string],
  newPpg: PropTypes.oneOf[number,string],
  tradePpg: PropTypes.oneOf[number,string],
  milesDriven: PropTypes.oneOf[number,string],
  data: object(),
  videos: object()
});
