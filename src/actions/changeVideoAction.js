import * as types from '../constants/actionTypes';

export const changeVideo = (id) => ({
    type: types.CHANGE_VIDEO,
    id
});