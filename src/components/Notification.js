import React, { Component } from 'react';
import { Col, Button } from 'react-bootstrap';

export default class Notifications extends Component {

    render() {

        return (
            <Col md={12} lg={12} xs={12}>
                <div className="notification-container">
                    <span className="notification-message">3 new profiles</span>
                    <Button className="notification-button">check now</Button>
                    <i className="glyphicon glyphicon-remove notification-button--close" />
                </div>
            </Col>
        );
    }
}