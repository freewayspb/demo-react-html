import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import {Grid, Row} from 'react-bootstrap';
import Notifications from './Notification';
import ModelListPage from '../containers/ModelListPage';
import NotFoundPage from './NotFoundPage';

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.

class App extends React.Component {
    render() {
        return (
            <div>
                <Grid>
                    <Row>
                        <Notifications />
                    </Row>
                </Grid>
                    <Switch>
                        <Route exact path="/" component={ModelListPage} />
                        <Route component={NotFoundPage} />
                    </Switch>
            </div>

        );
    }
}

App.propTypes = {
    children: PropTypes.element
};

export default App;