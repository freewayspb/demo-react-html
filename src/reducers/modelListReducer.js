import {GET_VIDEO, CHANGE_VIDEO} from '../constants/actionTypes';
import initialState from './initialState';

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I'm using Object.assign to create a copy of current state
// and update values on the copy.
export default function modelListReducer(state = initialState, action) {
    // let newState;

    switch (action.type) {
        case GET_VIDEO:
            return {...state, data: action.data, currentVideo: action.data.videos[0]};
        case CHANGE_VIDEO:
            return {...state, currentVideo: state.data.videos[action.id-1]};
        default:
            return state;
    }
}