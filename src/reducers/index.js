// Set up your root reducer here...
import { combineReducers } from 'redux';
import modelListReducer from './modelListReducer';
import { routerReducer } from 'react-router-redux';
const rootReducer = combineReducers({
    modelListReducer,
    routing: routerReducer
});
export default rootReducer;